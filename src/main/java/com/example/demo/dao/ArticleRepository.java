package com.example.demo.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long>{

	//


	@Query("select c from Article c where c.titre like :x")
	public Page<Article> chercher(@Param("x")String mc,Pageable pageable);
	
	
	
	
}
