package com.example.demo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.dao.ArticleRepository;
import com.example.demo.entities.Article;

@SpringBootApplication
public class SpringMvCrestfulBackEndApplication implements CommandLineRunner {
	@Autowired
private ArticleRepository ArticleRepository;
	public static void main(String[] args) {
		SpringApplication.run(SpringMvCrestfulBackEndApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		ArticleRepository.save(new Article("dacia","logan","https://www.cdn.daciagroup.com/content/dam/Dacia/ma/vehicles/logan/l52-logan/l52-logan-ph1/more-dacia/ambiance-logan.jpg.ximg.l_full_m.smart.jpg",false));
		ArticleRepository.save(new Article("peugeot","306","https://www.leseco.ma/images/stories/Peugeot_Rifter.jpg",true));
		
		ArticleRepository.save(new Article("mercedes","207","https://www.saga-mercedes-benz.com/media/thumbs/list/658531-0d4e554aad1cb214cfb2b3ceca5fcc8643cf34ed.png",true));
		ArticleRepository.save(new Article("dacia","duster","https://www.cdn.daciagroup.com/content/dam/Dacia/ma/vehicles/logan/l52-logan/l52-logan-ph1/more-dacia/ambiance-logan.jpg.ximg.l_full_m.smart.jpg",true));
		ArticleRepository.save(new Article("peugeot","407","https://www.challenge.ma/wp-content/uploads/2018/09/Peugeot-3008.jpg",false));
		ArticleRepository.save(new Article("hyunday","santafé 11","https://www.hyundai.com/content/dam/hyundai/ma/fr/data/vehicle-thumbnail/product/tucson/default/2.jpg",false));
		ArticleRepository.save(new Article("volswagen","golf4","https://www.rentmaroc.com/images/cars/1421418255.jpg",false));
		ArticleRepository.save(new Article("dacia","duster","https://www.cdn.daciagroup.com/content/dam/Dacia/ma/vehicles/logan/l52-logan/l52-logan-ph1/more-dacia/ambiance-logan.jpg.ximg.l_full_m.smart.jpg",false));
		ArticleRepository.save(new Article("mercedes","210","https://www.saga-mercedes-benz.com/media/thumbs/list/658531-0d4e554aad1cb214cfb2b3ceca5fcc8643cf34ed.png",false));
		ArticleRepository.save(new Article("toyota","hilux","https://images.toyota-europe.com/be/product-token/ea3c36b8-0d1a-46f5-adb2-b9916334aa4a/width/1200/exterior-3.jpg",false));
		ArticleRepository.save(new Article("volswagen","golf7","https://www.rentmaroc.com/images/cars/1421418255.jpg",false));

	}
	
	
	
	
	
	
	
	
}
