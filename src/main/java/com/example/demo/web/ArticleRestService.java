package com.example.demo.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ArticleRepository;
import com.example.demo.entities.Article;

@RestController
@CrossOrigin("*")
public class ArticleRestService {

	
	@Autowired
	private ArticleRepository articleRepository;
	
	@RequestMapping(value="/articles",method=RequestMethod.GET)
	public List<Article> getArticles(){
		return articleRepository.findAll();
	}
	
	
	@RequestMapping(value="/articles/{id}",method=RequestMethod.GET)
	public Article getContact(@PathVariable Long id){
		return articleRepository.findOne(id);
		
	}
	
	
	@RequestMapping(value="/articles/{id}",method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id){
		 articleRepository.delete(id);
return true;
		
	}
	
	
	
	@RequestMapping(value="/chercherArticle",method=RequestMethod.GET)
	public Page<Article> chercher(
			@RequestParam(name="mc",defaultValue="")String mc,
			@RequestParam(name="page",defaultValue="0")int page,
			@RequestParam(name="size",defaultValue="10")int size){
		return articleRepository.chercher("%"+mc+"%", new PageRequest(page, size));
	}
	
	
	
	@RequestMapping(value="/article",method=RequestMethod.POST)
	public Article save(@RequestBody Article c){
		return articleRepository.save(c);
		
	}
	
	@RequestMapping(value="/articles/{id}",method=RequestMethod.PUT)
	public Article save(@PathVariable Long id,@RequestBody Article c){
		c.setId(id);
		return articleRepository.save(c);
		
	}
	
	
	
}
