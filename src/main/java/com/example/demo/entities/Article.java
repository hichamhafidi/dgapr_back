package com.example.demo.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Article implements Serializable{
	@Id @GeneratedValue
	private Long id;
	private String titre;
	

	//this my first commit to gitLab
	//ahmed's first commit

	private String description;

	
	
	//this my first commit to gitLab    
	       
	private String photo;
	private boolean selected;
	public Article() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Article(String titre, String description, String photo, boolean selected) {
		super();
		this.titre = titre;
		this.description = description;
		this.photo = photo;
		this.selected = selected;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
